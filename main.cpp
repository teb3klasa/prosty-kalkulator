#include <iostream>
#include <iomanip>

//wybranie domyślnej przestrzeni nazw; dzięki tej operacji
//w kodzie nie trzeba będzie np. pisać
//std::cout << "Tekst";
//wystarczy
//cout << "Tekst";
using namespace std;

/*
 * Prosty kalkulator założenia:
 * - podstawowe operacje arytmetyczne (dodawanie, odejmowanie,
 *   mnożenie, dzielenie)
 * - możliwość działania na większej ilości danych (nie tylko dwóch)
 * - wykonywanie rozbudowanych działań
 * - dodatkowe działania na liczbach (silnia, modulo, potęga,
 *   pierwiastek)
 * - możliwość zapisywania i wczytywania wyników (zapis/odczyt plików)
 */

/* tak wygląda deklaracja funkcji w C++. Dzięki temu można używać
 * nazw poniższych funkcji w funkcji main(), sama zaś definicja
 * tychże funkcji została podana poniżej ciała funkcji main()
 *
 * - deklaracja - utworzenie zmiennej/funkcji/klasy poprzez podanie
 * jej nazwy oraz (w przypadku funkcji/metod) przyjmowanych parametrów,
 * brak natomiast instrukcji wykonywanych w ramach powyższej deklaracji
 *
 * - definicja - określenie wartości bądź działania utworzonej wcześniej
 * deklaracji; w przypadku funkcji mogą to być dowolne rozkazy/instrukcje
 *
 * Obie części można tworzyć jednocześnie, np.:
 *
 * int a = 15;
 * float b = a / 2;
 *
 * void wypiszTekst(std::string t) {
 *     std::cout << "Podany tekst to: " << t << endl;
 * }
 * */
int dzialanie();
int dzialanie2();

int main()
{
    while(1) {
        /* Poniższy kod został objęty komentarzem
         * gdyż jego funkcjonalność przejęła funkcja
         * dzialanie2();
         * */
// rozwiazanie numer 1
//        int w = dzialanie();
//        if (w == -1)
//            return -1;
// rozwiazanie numer 2
//        if (dzialanie() == -1)
//            return -1;
// rozwiazanie numer 3
//        if (dzialanie() == -1) break;
        /* koniec działań przejętych przez dzialanie2() */

        dzialanie2();
    }
    return 0;
}

/* zdefiniowanie działania funkcji dzialanie()
 * Funkcja ma za zadanie zaprezentować podstawowe działanie funkcji
 * jako takich; docelowo w przykładzie będzie wykorzystywana funkcja
 * dzialanie2()
 * */
int dzialanie() {
    float a=0,b=0;
    char z = '+';
    cout << "Podaj dzialanie: ";
    cin >> z;
    cout << "Podaj dwie liczby: ";
    cin >> a >> b;
    //jezeli z przechowuje rownowartosc znaku +
    if (z == '+')
        a += b; //a = a + b;
    //jezeli z przechowuje rownowartosc znaku -
    else if (z == '-')
        a -= b; //a = a - b;
    //jezeli z przechowuje rownowartosc znaku *
    else if (z == '*')
        a *= b; //a = a * b;
    //jezeli z przechowuje rownowartosc znaku /
    // ORAZ
    // wartosc b jest inna niż 0 (może być dowolna dodatnia
    // lub ujemna)
    else if (z == '/' && b != 0)
        a /= b; //a = a / b;
    else {
        cout << "Nieznana operacja " <<
                "arytmetyczna badz blad dzielenia!";
        return -1;
    }

    cout << endl << "Wynik dzialania: " << a << endl;
    return 0;
}
/* DO WYKONANIA:
 * - a i b powinno być przekazywane jako parametr funkcji, także
 *   symbol dzialania!
 * - co za tym idze część pytająca o działanie powinna znajdować się
 *   w innej funkcji (można wykorzystać main)
 * - dodać działania: silania, potęga, pierwiastek, modulo
 * - dodać znak, który będzie kończył działanie programu
 * */

int dzialanie2() {
    float a=0,b=0;
    char z = '+';
    cout << "Podaj dzialanie: ";
    cin >> z;
    cout << "Podaj dwie liczby: ";
    cin >> a >> b;
    /* seria if..else zastąpiona poprzez przełącznik (switch)
     * wygodny do użycia, trzeba pamiętać o słowie break po każdym
     * przypadku (chyba, że od trafnego przypadku mają się wykonać
     * wszystkie pozostałe przypadki, występujące po nim
     * */
    switch(z) {
        case '+': a+=b; break;
        case '-': a-=b; break;
        case '*': a*=b; break;
        case '/': a/=b; break;
        default: cout << "Nie rozpoznano dzialania!";
    }
    cout << "Wynik dzialania: " << a << endl;
}
